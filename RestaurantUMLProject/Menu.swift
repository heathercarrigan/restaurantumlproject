//
//  Menu.swift
//  RestaurantUMLProject
//
//  Created by Heather Carrigan on 07/03/2022.
//

import Foundation

public class Menu {
    private var items: Array<String>
    private var title: String
    
    public init(items: Array<String>, title: String) {
        self.items = items
        self.title = title
    }
    
    public func getItems() -> Array<String> {
        return items
    }
    
    public func getTitle() -> String {
        return title
    }
}
