//
//  Item.swift
//  RestaurantUMLProject
//
//  Created by Heather Carrigan on 07/03/2022.
//

import Foundation

public class Item {
    private var name: String
    private var price: Double
    
    public init(name: String, price: Double) {
        self.name = name
        self.price = price
    }
    
    public func getName() -> String {
        return name
    }
    
    public func getPrice() -> Double {
        return price 
    }
}
