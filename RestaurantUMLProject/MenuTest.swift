//
//  MenuTest.swift
//  RestaurantUMLProjectTests
//
//  Created by Heather Carrigan on 07/03/2022.
//

import XCTest
@testable import RestaurantUMLProject

public class MenuTest: XCTestCase {
    
    public func testMenu() -> Void {
        let menu = Menu(items: ["Goats Cheese", "Steak", "Lemon Tart", "White Wine"], title: "A La Carte")
        
        XCTAssertEqual(menu.getItems(), ["Goats Cheese", "Steak", "Lemon Tart", "White Wine"])
        XCTAssertEqual(menu.getTitle(), "A La Carte")
    }
}
