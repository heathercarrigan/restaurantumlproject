//
//  RestaurantTest.swift
//  RestaurantUMLProjectTests
//
//  Created by Heather Carrigan on 07/03/2022.
//

import XCTest
@testable import RestaurantUMLProject

public class RestaurantTest: XCTestCase {
    
    public func testRestaurant() -> Void {
        let restaurant = Restaurant(name: "Heathers Place", imageURL: "WebURL", menus: ["Starters", "Mains", "Desserts", "Drinks"])
        
        XCTAssertEqual(restaurant.getName(), "Heathers Place")
        XCTAssertEqual(restaurant.getImageURL(), "WebURL")
        XCTAssertEqual(restaurant.getMenus(), ["Starters", "Mains", "Desserts", "Drinks"])
    }
}
