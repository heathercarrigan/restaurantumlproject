//
//  ItemTest.swift
//  RestaurantUMLProjectTests
//
//  Created by Heather Carrigan on 07/03/2022.
//

import XCTest
@testable import RestaurantUMLProject

public class ItemTest: XCTestCase {

    public func testItem() -> Void {
        let item = Item(name: "Goats Cheese", price: 4.95)
        
        XCTAssertEqual(item.getName(), "Goats Cheese")
        XCTAssertEqual(item.getPrice(), 4.95)
    }
}
