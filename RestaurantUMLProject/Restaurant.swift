//
//  Restaurant.swift
//  RestaurantUMLProject
//
//  Created by Heather Carrigan on 07/03/2022.
//

import Foundation

public class Restaurant {
    private var name: String
    private var imageURL: String
    private var menus: Array<String>
    
    public init(name: String, imageURL: String, menus: Array<String>) {
        self.name = name
        self.imageURL = imageURL
        self.menus = menus
    }
    
    public func getName() -> String {
        return name
    }
    
    public func getImageURL() -> String {
        return imageURL
    }
    
    public func getMenus() -> Array<String> {
        return menus
    }
}
