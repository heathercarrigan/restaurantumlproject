//
//  RestaurantUMLProjectApp.swift
//  RestaurantUMLProject
//
//  Created by Heather Carrigan on 07/03/2022.
//

import SwiftUI

@main
struct RestaurantUMLProjectApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
